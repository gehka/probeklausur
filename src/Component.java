public interface Component {
    void accept(Visitor v);
}
