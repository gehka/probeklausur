import java.util.ArrayList;
import java.util.List;

public class Composite implements Component{
    private List<Component> contents;

    public Composite() {
        this.contents = new ArrayList<Component>();
    }

    public void add(Component component) {
        this.contents.add(component);
    }

    public List<Component> getContents() {
        return this.contents;
    }

    @Override
    public void accept(Visitor v) {
        v.handle(this);
    }
}
