import java.util.ArrayList;
import java.util.List;

public class Collector implements Visitor {

    private List<String> leafNames;

    public Collector() {
        this.leafNames = new ArrayList<>();
    }

    @Override
    public void handle(Composite c) {
        for (Component current : c.getContents()) {
            current.accept(this);
        }
    }

    @Override
    public void handle(Leaf l) {
        leafNames.add(l.getName());
    }

    public List<String> getListOfLeafNames(Composite c) {
        c.accept(this);
        return this.leafNames;
    }
}
