public class ContainsVisitor implements Visitor {

    private boolean contains = false;
    private Component y;

    public ContainsVisitor() {

    }

    @Override
    public void handle(Composite c) {
        if(c==y)
            this.contains = true;
        else
            this.contains(c, y);
    }

    @Override
    public void handle(Leaf l) {
        if (l == y)
            contains = true;
    }

    public boolean contains(Composite x, Component y) {
        this.y = y;
        for(Component current : x.getContents()) {
            current.accept(this);
        }

        return this.contains;
    }
}
