public interface Visitor {

    void handle(Composite c);
    void handle(Leaf l);
}
