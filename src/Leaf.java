public class Leaf implements Component {

    private String name;

    public Leaf(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void accept(Visitor v) {
        v.handle(this);
    }
}
