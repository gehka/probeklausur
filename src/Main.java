public class Main {

    public static void main(String[] args) {
     /*   Composite composite1 = new Composite();
        Composite composite2 = new Composite();
        Composite composite3 = new Composite();
        Leaf leaf1 = new Leaf("Leaf1");
        Leaf leaf2 = new Leaf("Leaf2");
        Leaf leaf3 = new Leaf("Leaf3");
        Leaf leaf4 = new Leaf("Leaf4");
        Leaf leaf5 = new Leaf("Leaf5");

        composite1.add(leaf1);
        composite1.add(leaf2);

        composite2.add(leaf3);
        composite2.add(leaf4);
        composite2.add(leaf5);

        composite2.add(composite3);
        composite1.add(composite2);

        Collector collector = new Collector();

        System.out.println(collector.getListOfLeafNames(composite1));

        ContainsVisitor containsVisitor = new ContainsVisitor();
        System.out.println(containsVisitor.contains(composite1, composite3));
*/

     Composite c1, c2, c3, c4, c5;
     Leaf l1, l2, l3, l4, l5;
     c1 = new Composite();
     c2 = new Composite();
     c3 = new Composite();
     c4 = new Composite();
     c5 = new Composite();

     l1 = new Leaf("a1");
     l2 = new Leaf("a2");
     l3 = new Leaf("a3");
     l4 = new Leaf("a4");
     l5 = new Leaf("a5");

     c5.add(l5);
     c2.add(c5);
     c2.add(l2);
     c1.add(c2);
     c1.add(l1);
     c3.add(l3);
     c3.add(l4);
     c1.add(c3);
     c1.add(c4);

     Collector collector = new Collector();
        System.out.println(collector.getListOfLeafNames(c1));
     ContainsVisitor containsVisitor = new ContainsVisitor();
     System.out.println(containsVisitor.contains(c1, l5));
    }
}
